/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package org.vertexprize.education.nn.neuroph;

import java.util.Arrays;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSetRow;

/**
 *
 * @author vaganovdv
 */
public class NetworkExample {

    public static void main(String[] args) {
        
        NetworkBuilder builder = new NetworkBuilder();
        NeuralNetwork model = builder.build();
        
        NetworkTrainer trainer = new NetworkTrainer(model);
        NeuralNetwork network = trainer.train();
        
        DataSetRow in = new DataSetRow(new double[] { 1, 1 });
        
        network.setInput(in.getInput());
        network.calculate();
        double[] output = network.getOutput();
        
        System.out.print("Input: " + Arrays.toString(in.getInput()) );
        System.out.println(" Output: " + Arrays.toString(output) );
        
        
        
    }
}
